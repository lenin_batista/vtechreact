import React,{useContext} from 'react'
import {
    BrowserRouter as Router,
    Switch
  } from "react-router-dom";

  import { UserContext } from '../Hooks/UserContext'

import Auth from '../components/auth/Auth';
import TaskList from '../components/task/TaskList';


import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

export default function AppRouter() {

    const userContext = useContext(UserContext)
    console.log("AppRouter",userContext.stateUser.isLoged)
    return (
        <Router>
            <div>
            <Switch> 
                    <PublicRoute 
                        exact 
                        path="/" 
                        component={ Auth } 
                        isAuthenticated={ userContext.stateUser.isLoged }
                    />
                    
                    <PrivateRoute 
                        path="/task" 
                        component={ TaskList } 
                        isAuthenticated={ userContext.stateUser.isLoged }
                    />
                </Switch>
            </div>            
        </Router>
    )
}
