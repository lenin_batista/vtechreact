import React, { useState, useContext } from 'react'
import '../../assets/css/style.css'
import { UserContext } from '../../Hooks/UserContext'

export default function Auth() {

    const userContext = useContext(UserContext)

    const url = 'http://localhost:8001'
    const [useForm, setForm] = useState({
        email: '',
        passWord: ''
    })

    const [useErrorLogin, setUseErrorLogin] = useState(false)

    const { email, passWord } = useForm

    const onHandledClick = async (e) => {
        e.preventDefault()
        const { response } = await onLogin()

        if (!response || response === undefined) {
            setUseErrorLogin(true)
            return
        }
        console.log("response", response)
        localStorage.setItem('user', JSON.stringify({
            isLoged: true,
            user: response

        }));
        userContext.setStateUser({
            isLoged: true,
            user: response,

        })
        //console.log(await onLogin())

    }

    const onChangeInput = ({ target }) => {

        setForm({
            ...useForm,
            [target.name]: target.value
        })
    }


    const onLogin = async () => {
        const rawResponse = await fetch(`${url}/api/user/auth`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(useForm)
        })
        const content = await rawResponse.json()

        return content
    }

    const msgErrorLogin = () => {

        setTimeout(() => {
            setUseErrorLogin(false)
        }, 3000)
        return (
            <div className="alert alert-danger mt-6" role="alert">
                Ivalid email o password
            </div>
        )
    }

    return (
        <>
        <div className="d-flex justify-content-center">
            <div className="mt-5">
                <form >
                    <div className="card ">
                        <div className="card-body content-center">
                            <div className="mb-3">
                                <label className="form-label">Email address</label>
                                <input
                                    className="form-control"
                                    type="text"
                                    name="email"
                                    value={email}
                                    onChange={onChangeInput}
                                />
                            </div>
                            <div className="form-group">
                                <label className="form-label">Password</label>
                                <input
                                    className="form-control"
                                    type="password"
                                    name="passWord"
                                    value={passWord}
                                    onChange={onChangeInput}
                                />
                            </div>

                            <button className="btn btn-primary btn-block" onClick={onHandledClick}> Login </button>

                        </div>
                    </div>
                </form>
            </div>

            

        </div>
        {useErrorLogin && msgErrorLogin()}
        </>
    )
}
