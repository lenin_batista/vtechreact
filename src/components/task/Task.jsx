import React from 'react'

const Task = React.memo(({handleListState,handleDeleteTask,index,item}) => {
    const {id,state,name} = item
    const className = state ? "d-flex list-group-item bg-success" : " d-flex list-group-item "
    return (
        <>
            <li className={className} key={id} >
                <div className="p-2 w-100">{name}</div>
                <div className=" p-2 flex-shrink-1 btn-group btn-group-sm" role="group" aria-label="Basic example">
                    { !state && (<button onClick={() => handleListState(index, id)} className="btn btn-primary">Done</button>) }
                    <button onClick={() => handleDeleteTask( id)} className="btn btn-danger">Delete</button>
                </div>

            </li>
        </>
    )
})

export default Task