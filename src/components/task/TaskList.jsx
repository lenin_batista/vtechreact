import React, { useCallback, useEffect, useState, useContext } from "react";
import Task from "./Task";
import TaskForm from "./TaskForm";
import { UserContext } from '../../Hooks/UserContext'

const TodoList = ({ history }) => {
    const userContext = useContext(UserContext)
    const { token } = userContext.stateUser.user
    const url = 'http://localhost:8001'
    const [newToDo, setNewToDo] = useState([]);
    const [statusPage, setStatusPage] = useState({
        isLoaded: false,
        isError: false,
        errorMsg: ''
    })

    const { isLoaded, errorMsg } = statusPage

    const handleListState = useCallback((index, id) => {
        fetch(`${url}/api/task/${id}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            }
        })
            .then(response => response.json())
            .then(data => {
                setNewToDo(newTodo => {
                    newTodo[index] = data
                    return [...newTodo]
                });
            })

    }, [setNewToDo, token]);

    const handleNewTodo = useCallback((toDo) => {
        fetch(`${url}/api/task/`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
            body: JSON.stringify(toDo)
        })
            .then(response => response.json())
            .then(data => {

                setNewToDo(newTodo => {
                    newTodo.push(data);
                    return [...newTodo]
                });
            })
    }, [setNewToDo, token]);

    const handleDeleteTask = useCallback((id) => {
        fetch(`${url}/api/task/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,

            }
        })
            .then(response => response.json())
            .then(resp => {
                setNewToDo(tasks => {
                    const taskList = tasks.filter((item) => item.id !== id)
                    return [...taskList]
                })
            })

    }, [token])


    const getAllTask = useCallback(async () => {
        return (fetch(`${url}/api/task/`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
            .then(response => response.json())
            .then(data => {
                console.log("data", data)
                if (data.msg) {
                    fnSetStatus(false, true, data.msg)
                    return []
                } else {
                    fnSetStatus(true, false, '')
                    return data
                }
            }))
            .catch(error => {
                console.log(error)

            })
    }, [token])

    const fnSetStatus = (loaded, error, message) => {
        setStatusPage({
            isLoaded: loaded,
            isError: error,
            errorMsg: message
        })
    }

    const fnLogout = () => {
        userContext.setStateUser({
            isLoged: false,
            user: null,
        })
    }

    useEffect(() => {
        getAllTask().then(data => {
            setNewToDo(data)
        })
    }, [getAllTask])


    return (

        <>
            <div className="d-flex flex-row-reverse ">
                <button className="btn btn-default" onClick={fnLogout}> Log out</button>
            </div>
            <div className="d-flex justify-content-center">


                
                {!isLoaded ?
                    (
                        <div className="alert alert-danger mt-6" role="alert">
                            {errorMsg}
                        </div>
                    )

                    :

                    (
                        <div >
                            <div >
                                <TaskForm handleNewTodo={handleNewTodo} />
                            </div>
                            <div className="card content-center" >
                                <div className="card-header">
                                    TODO
                                </div>
                                <ul className="list-group list-group-flush">
                                    {newToDo.map((item, index) => {
                                        return (
                                            <Task
                                                key={item.id}
                                                handleListState={handleListState}
                                                handleDeleteTask={handleDeleteTask}
                                                index={index}
                                                item={item}
                                            />
                                        );
                                    })}
                                </ul>

                            </div>
                        </div>
                    )

                }



            </div>
            <div clasName="d-flex">

            </div>

        </>
    );
};

export default TodoList;
