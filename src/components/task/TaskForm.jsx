import React, { useState } from "react";

const FormTodo = ({ handleNewTodo }) => {
  const [toDoName, setToDoName] = useState("");

  const handleClick = () => {
    if (toDoName) {
      handleNewTodo({
        name: toDoName
      });

      setToDoName("");
    }
  };
  return (
    <>
      <div className="input-group mb-3 mt-5">
        <input
          className="form-control"
          name="newTodo"
          placeholder="Todo..."
          value={toDoName}
          onChange={({ target }) => setToDoName(target.value)}
        />
        <div className="input-group-append">
          <button className="btn btn-outline-secondary" onClick={handleClick}> new todo </button>
        </div>
      </div>
    </>
  );
};

export default FormTodo;
