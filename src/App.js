import React, { useState, useEffect } from "react";



import { UserContext } from "./Hooks/UserContext";
import AppRouter from "./routers/AppRouter";



function App() {

  const [stateUser, setStateUser] = useState({
    isLoged: false,
    user: null,
  })

  useEffect(() => {
    const userLog = JSON.parse(localStorage.getItem('user')) || {
      isLoged: false,
      user: null,
    };
    setStateUser(userLog)

  }, [])

  

  return (
    <>
      <UserContext.Provider value={{ stateUser, setStateUser }} >
        <AppRouter />
      </UserContext.Provider>
    </>
  );
}

export default App;

